package com.amx.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.amx.qa.base.TestBase;

public class ForgotPassword extends TestBase{

	@FindBy(xpath="//h4[contains(text(),'Verify OTP')]")
	WebElement verifyOTP;
	
	
	// Initializing the Page Objects:
		public ForgotPassword() {
			PageFactory.initElements(driver, this);
		}
	
	public boolean validateVerifyOTPLabel(){
		return verifyOTP.isDisplayed();
	}
	
	
	
}
