package com.amx.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.amx.qa.base.TestBase;

public class SignUpPage extends TestBase{
	
	@FindBy(xpath="//h4[contains(text(),'Sign up')]")
	WebElement signUpLabel;
	
	
	// Initializing the Page Objects:
		public SignUpPage() {
			PageFactory.initElements(driver, this);
		}
	
	public boolean validateSignUpLabel(){
		return signUpLabel.isDisplayed();
	}
	

}
