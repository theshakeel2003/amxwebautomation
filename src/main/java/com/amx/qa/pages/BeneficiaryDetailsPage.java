package com.amx.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.amx.qa.base.TestBase;
import com.amx.qa.util.TestUtil;

public class BeneficiaryDetailsPage extends TestBase {

	@FindBy(xpath = "//label[contains(text(),'First Name')]/../input")
	WebElement FirstNameWebElement;

	@FindBy(xpath = "//label[contains(text(),'Last Name')]/../input")
	WebElement LastNameWebElement;

	@FindBy(xpath = "//label[contains(text(),'Relationship')]/following::input[1]")
	WebElement RelationShipWebElement;

	@FindBy(xpath = "//label[contains(text(),'Nationality')]/following::div[@class='Select-control'][1]")
	WebElement NationalityWebElement;

	@FindBy(xpath = "//label[contains(text(),'Country')]/following::div[@class='Select-control'][1]")
	WebElement CountryWebElement;

	@FindBy(xpath = "//label[contains(text(),'State')]/following::div[@class='Select-control'][1]")
	WebElement StateWebElement;

	@FindBy(xpath = "//label[contains(text(),'District')]/following::div[@class='Select-control'][1]")
	WebElement DistrictWebElement;

	@FindBy(xpath = "//label[contains(text(),'Mobile Number')]/following::div[@class='Select-control'][1]")
	WebElement MobileCountryCodeWebElement;

	@FindBy(xpath = "//label[contains(text(),'Mobile Number')]/following::div[contains(@class,'floating-label-input')]")
	WebElement MobileNumberWebElement;

	@FindBy(xpath = "//button[text()='Next']")
	WebElement NextButtonWebElement;

	// Initializing the Page Objects:
	public BeneficiaryDetailsPage() {
		PageFactory.initElements(driver, this);
	}

	public void selectBeneficiaryType(String beneficiaryType) {
		

	//	String innertext= driver.findElement(By.xpath("//label[@class='radioLabel' and @for='Others']")).getAttribute("innerHTML") ;

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("arguments[0].click();", driver.findElement(By.xpath("//label[@class='radioLabel' and @for='Others']"))); 
		System.out.println("Others clicked");

	}

	public void createBeneficiaryDetails(String firstName, String lastName, String relationShip, String nationality,
			String country, String state, String district, String mobilecountryCode, String mobileNumber)
			throws InterruptedException {

		

		if (relationShip.equalsIgnoreCase("OTHERS")) {
			
			TestUtil.selectDropDownValues(FirstNameWebElement, firstName);
			TestUtil.selectDropDownValues(LastNameWebElement, lastName);
			TestUtil.selectDropDownValues(RelationShipWebElement, relationShip);
		}

		TestUtil.selectDropDownValues(NationalityWebElement, nationality);
		TestUtil.selectDropDownValues(CountryWebElement, country);
		TestUtil.selectDropDownValues(StateWebElement, state);
		TestUtil.selectDropDownValues(DistrictWebElement, district);
		TestUtil.selectDropDownValues(MobileCountryCodeWebElement, mobilecountryCode);
		
	//	MobileNumberWebElement.sendKeys(mobileNumber);
		
		Actions actions = new Actions(driver);
			actions.moveToElement(MobileNumberWebElement);
			actions.click();
			
			System.out.println("Mobile no entered");
		
		//TestUtil.selectDropDownValues(MobileNumberWebElement, mobileNumber);

	}

	public VerifyOTPPage clickOnNextButton() {

		NextButtonWebElement.click();
		return new VerifyOTPPage();
	}

}