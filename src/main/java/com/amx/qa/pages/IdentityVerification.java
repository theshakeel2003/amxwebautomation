package com.amx.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.amx.qa.base.TestBase;
import com.amx.qa.util.DropDownUtility;
import com.amx.qa.util.TextFieldUtility;
import com.amx.qa.util.VerificationUtility;
import com.amx.qa.util.WaitUtility;

public class IdentityVerification extends TestBase {
	
	WaitUtility waitUtility;
	DropDownUtility dropDownUtility;
	VerificationUtility verificationUtility;
	TextFieldUtility textFieldUtility;

	@FindBy(xpath = "//input[@id='securityQuestion']")
	WebElement securityQuestion;
	
	@FindBy(xpath = "//button[text()='Next']")
	WebElement nextButton;
	
	@FindBy(xpath = "//button[text()='Cancel']")
	WebElement cancelButton;
	
	@FindBy(xpath = "//button[text()='Send OTP']")
	WebElement sendOtpButton;
	
	
	
	// Initializing the Page Objects:
	public IdentityVerification() {
		PageFactory.initElements(driver, this);
		waitUtility=new WaitUtility(driver);
		dropDownUtility=new DropDownUtility(driver);
		verificationUtility=new VerificationUtility(driver);
		textFieldUtility=new TextFieldUtility(driver);
	}
	
	
public void enterSecurityAnswer(String securityAnswer){
		
		securityQuestion.sendKeys(securityAnswer);
	}
	
	public RemittanceDetails clickOnNextButton() {
		nextButton.click();
		return new RemittanceDetails();
	}
	

	public void clickOnCancelButton() {
		cancelButton.click();
	}
	
	public void clickOnSendOTPButton() {
		sendOtpButton.click();
	}
	
}
