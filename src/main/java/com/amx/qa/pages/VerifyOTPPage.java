package com.amx.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.amx.qa.base.TestBase;

public class VerifyOTPPage extends TestBase {

	@FindBy(xpath = "//input[@id='otp']")
	WebElement mobileOTP;

	@FindBy(xpath = "//input[@id='e-otp']")
	WebElement emailOTP;
	
	

	// Initializing the Page Objects:
	public VerifyOTPPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	
}
