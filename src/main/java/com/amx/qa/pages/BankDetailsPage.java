package com.amx.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.amx.qa.base.TestBase;
import com.amx.qa.util.TestUtil;

public class BankDetailsPage extends TestBase {

	@FindBy(xpath = "//label[contains(text(),'Bank')]/following::div[1]")
	WebElement BankWebElement;

	@FindBy(xpath = "//label[contains(text(),'Currency')]/following::div[1]")
	WebElement CurrencyWebElement;

	@FindBy(xpath = "//label[contains(text(),'Branch')]/following::label[1]")
	WebElement branchSearchWebElement;

	@FindBy(xpath = "//input[@type='text' and @data-type='custom-pwd']")
	WebElement AccountNoWebElement;

	@FindBy(xpath = "//div[@class='floating-label-input retype-account ']/input[@type='text']")
	WebElement ReTypeAccountNoWebElement;

	@FindBy(xpath = "//label[contains(text(),'Account Type')]/following::div[1]")
	WebElement AccountTypeWebElement;

	@FindBy(xpath = "//button[text()='Next']")
	WebElement NextButtonWebElement;

	// Initializing the Page Objects:
	public BankDetailsPage() {
		PageFactory.initElements(driver, this);
	}

	
	
	
	
	public void createBanDetails(String beneficiaryBank,String beneficiaryCurrency,String beneficiaryAccountNo,String beneficiaryReAccountNo,String beneficiaryAccountType ) throws InterruptedException 
	{
		
		
		TestUtil.selectBankDropDownValues(BankWebElement, beneficiaryBank);
		TestUtil.selectDropDownCurrencyValues(CurrencyWebElement, beneficiaryCurrency);

		//		CurrencyWebElement.sendKeys(beneficiaryCurrency);
	//	JavascriptExecutor js = (JavascriptExecutor) driver; 
	//	System.out.println("java script");
	//	js.executeScript("document.getElementByXPath('//label[contains(text(),'Currency')]/following::div[1]').value='USD -Us Dollar';");
	

//		AccountNoWebElement.sendKeys(beneficiaryAccountNo);
		TestUtil.sendKeys(driver, AccountNoWebElement, 10, beneficiaryAccountNo);
		TestUtil.sendKeys(driver, ReTypeAccountNoWebElement, 10, beneficiaryReAccountNo);
//		ReTypeAccountNoWebElement.sendKeys(beneficiaryReAccountNo);
		TestUtil.selectAccountTypeDropDownValues(AccountTypeWebElement, beneficiaryAccountType);
	}
	
	
	public SearchBranchPage clickOnSearchBranch() {

		branchSearchWebElement.click();
		return new SearchBranchPage();
	}
	
	

	public BeneficiaryDetailsPage clickOnNextButton() {

		NextButtonWebElement.click();
		return new BeneficiaryDetailsPage();
	}
	
	
	/*
	 * public void selectContactsByName(String name){
	 * driver.findElement(By.xpath("//a[text()='"+name+
	 * "']//parent::td[@class='datalistrow']" +
	 * "//preceding-sibling::td[@class='datalistrow']//input[@name='contact_id']")).
	 * click(); }
	 * 
	 * 
	 * 
	 * 
	 */
}
