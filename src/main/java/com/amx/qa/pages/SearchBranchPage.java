package com.amx.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.amx.qa.base.TestBase;
import com.amx.qa.util.TestUtil;

public class SearchBranchPage extends TestBase {


	@FindBy(xpath = "//h4[contains(text(),'Search Branch')]/following::label[contains(text(),'Branch')][1]/../input")
	WebElement searchBranchWebElement;
	
	
	@FindBy(xpath = "//div[@data-label='Branch Full Name']")
	WebElement branchFullNameWebElement;
	
	@FindBy(xpath ="//div[@class='container loading-inline']")
	WebElement loaderWebElement;

	// Initializing the Page Objects:
	public SearchBranchPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	public void searchBranch(String branchName) throws InterruptedException {
		
		System.out.println("Select Branch");
		TestUtil.sendKeys(driver, searchBranchWebElement, 5, branchName);
		Thread.sleep(5000);
	//	TestUtil.waitForAnyObjectTillNotVisible(loaderWebElement);	
		System.out.println("Waited till not displayed");
}
public void selectBranch() throws InterruptedException {
	
	
//	TestUtil.waitForloader(driver, loaderWebElement, 10);
	TestUtil.onClick(driver, branchFullNameWebElement, 10);

	System.out.println("Waited till not displayed");
	
}

	
	
	

}
