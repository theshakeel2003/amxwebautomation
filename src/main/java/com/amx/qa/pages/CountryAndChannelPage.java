package com.amx.qa.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import com.amx.qa.base.TestBase;
import com.amx.qa.util.TestUtil;

public class CountryAndChannelPage extends TestBase {

	
//	//div[@class='select-box label country']
	
	@FindBy(xpath = "//h4[text()='Country and Channel']/following::div/input[@name='select-Country']/..")
	WebElement channelCountry;
	
	
	
	//@FindBy(xpath = "//div[contains(@id,'react-select')]")
	//WebElement channelCountry;
	
	@FindBy(xpath = "//button[text()='Next']")
	WebElement nextButton;

	// Initializing the Page Objects:
	public CountryAndChannelPage() {
		PageFactory.initElements(driver, this);
	}



	public void selectChannelCountry(String countryValue) throws InterruptedException {
		
		TestUtil.selectDropDownValues(channelCountry, countryValue);

	}
	
	
	public void selectChannel(String bankValue) throws InterruptedException {

		driver.findElement(By.xpath("//a[text()='"+bankValue+"']")).click();


	}
	
	public BankDetailsPage clickOnNextButton() {

		nextButton.click();
		return new BankDetailsPage();
	}
	
	

	
	
	
	
	
	
	
	/*
	 * public void selectContactsByName(String name){
	 * driver.findElement(By.xpath("//a[text()='"+name+
	 * "']//parent::td[@class='datalistrow']" +
	 * "//preceding-sibling::td[@class='datalistrow']//input[@name='contact_id']")).
	 * click(); }
	 * 
	 * 
	 * public void createNewContact(String title, String ftName, String ltName,
	 * String comp){ Select select = new
	 * Select(driver.findElement(By.name("title")));
	 * select.selectByVisibleText(title);
	 * 
	 * firstName.sendKeys(ftName); lastName.sendKeys(ltName);
	 * company.sendKeys(comp); saveBtn.click();
	 * 
	 * }
	 * 
	 */

}
