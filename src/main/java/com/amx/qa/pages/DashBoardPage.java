package com.amx.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.amx.qa.base.TestBase;

public class DashBoardPage extends TestBase {

	@FindBy(xpath = "//span[@class='last-login']")
	WebElement lastLogin;

	// a[@href='/app/landing/beneficiaries']

	@FindBy(xpath = "//div[@class='user-name']")
	WebElement userName;
	
	@FindBy(xpath = "//a[@href='/app/landing/remittance']")
	WebElement remittanceDetails;
	

	@FindBy(xpath = "//a[@href='/app/landing/my-transactions']")
	WebElement myTransactions;

	@FindBy(xpath = "//a[@href='/app/landing/beneficiaries']")
	WebElement viewBeneficiaries;


	@FindBy(xpath = "//a[@href='/app/landing/exchange-rate']")
	WebElement exchangeRate;

	// Initializing the Page Objects:
	public DashBoardPage() {
		PageFactory.initElements(driver, this);
	}

	public String verifyLastLoginText() {
		String lastLoginText = lastLogin.getAttribute("innerText");
		return lastLoginText;
	}

	public String verifyUserNameText() {

		String userNameText = userName.getAttribute("innerText");
		return userNameText;
	}

	public boolean verifyRemittenceDetailsLink() {

		boolean remittenceLink = remittanceDetails.isDisplayed();
		return remittenceLink;
	}
	
	
	public boolean verifyMyTransactionLink() {

		boolean myTransactionsLink = myTransactions.isDisplayed();
		return myTransactionsLink;
	}
	
	
	public boolean verifyViewBeneficiaryLink() {

		boolean viewBeneficiariesLink = viewBeneficiaries.isDisplayed();
		return viewBeneficiariesLink;
	}
	
	public boolean verifyViewExchangeRateLink() {

		boolean exchangeRateLink = exchangeRate.isDisplayed();
		return exchangeRateLink;
	}
	

	public BeneficiariesPage clickOnBeneficiaryLink() {

		viewBeneficiaries.click();
		return new BeneficiariesPage();
	}
	
	
	
	
}
