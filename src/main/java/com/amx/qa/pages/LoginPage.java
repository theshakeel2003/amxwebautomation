package com.amx.qa.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.amx.qa.base.TestBase;

public class LoginPage extends TestBase {

	// Page Factory - OR:
	@FindBy(xpath = "//input[@id='civil-id']")
	WebElement civilId;

	@FindBy(xpath = "//input[@id='password']")
	WebElement password;

	@FindBy(xpath = "//button[@class='button ripple'][contains(text(),'Login')]")
	WebElement loginBtn;

	@FindBy(xpath = "//button[contains(text(),'Sign up')]")
	WebElement signUpBtn;

	@FindBy(xpath = "//button[contains(text(),'Forgot Password')]")
	WebElement forgotPwdBtn;

	@FindBy(xpath = "//label[contains(@class,'checkbox')]")
	WebElement virtualKeyboard;

	// Initializing the Page Objects:
	public LoginPage() {
		PageFactory.initElements(driver, this);
	}

	// Actions:
	public String validateLoginPageTitle() {
		return driver.getTitle();
	}

	public DashBoardPage login(String civilid, String pwd) {
		civilId.sendKeys(civilid);
		password.sendKeys(pwd);
		loginBtn.click();
		return new DashBoardPage();
	}

	public SignUpPage clickOnSignUpButton()

	{
		signUpBtn.click();
		return new SignUpPage();

	}

	public ForgotPassword clickOnForgotPasswordButton()

	{
		forgotPwdBtn.click();
		return new ForgotPassword();

	}

}
