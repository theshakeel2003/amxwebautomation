package com.amx.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.amx.qa.base.TestBase;
import com.amx.qa.util.DropDownUtility;
import com.amx.qa.util.TestUtil;
import com.amx.qa.util.WaitUtility;

public class BillingInformation extends TestBase {
	
	DropDownUtility dropDownUtility;
	WaitUtility waitUtility;

	// Page Factory - OR:
	@FindBy(xpath = "//select[@onchange='CreatePreFix()']")
	WebElement billingBankName;

	@FindBy(xpath = "//select[@name='SelectPrefix']/following-sibling::input")
	WebElement billingCardNumber;

	@FindBy(xpath = "//select[@name='Ecom_Payment_Card_ExpDate_Month']")
	WebElement billingCardExpiryMonth;
	
	@FindBy(xpath = "//select[@name='Ecom_Payment_Card_ExpDate_Year']")
	WebElement billingCardExpiryYear;
	
	@FindBy(xpath = "//input[@id='Ecom_Payment_Pin_id']")
	WebElement billingCardPIN;
	
	@FindBy(xpath = "//input[@name='EntrySubmitAction']")
	WebElement billingSubmitButton;
	
	@FindBy(xpath = "//input[@name='ResetAction']")
	WebElement billingResetButton;
	
	@FindBy(xpath = "//input[@id='CancelAction_id']")
	WebElement billingCancelButton;
	
	@FindBy(xpath = "//input[@name='ConfirmAction']")
	WebElement billingConfirmActionButton;
	
	@FindBy(xpath = "//input[@name='BackAction']")
	WebElement billingBackActionButton;
	
	@FindBy(xpath = "//input[@name='CancelAction']")
	WebElement billingCancelActionButton;

	// Initializing the Page Objects:
	public BillingInformation() {
		PageFactory.initElements(driver, this);
		waitUtility=new WaitUtility(driver);
		dropDownUtility=new DropDownUtility(driver);
	}


	public void selectBankName(String bankName){
		
		waitUtility.waitForElement(billingBankName, TestUtil.IMPLICIT_WAIT);
		dropDownUtility.selectUsingVisibleText(billingBankName, bankName);
	}

	public void enterCardNumber(String cardNumber){
		
		waitUtility.waitForElement(billingCardNumber, TestUtil.IMPLICIT_WAIT);
		billingCardNumber.sendKeys(cardNumber);
	
	}
	
	public void selectCardExpiryMonthAndYear(String expiryMonth,String expiryYear){
		
		waitUtility.waitForElement(billingCardExpiryMonth, TestUtil.IMPLICIT_WAIT);
		dropDownUtility.selectUsingVisibleText(billingCardExpiryMonth, expiryMonth);
		dropDownUtility.selectUsingVisibleText(billingCardExpiryYear, expiryYear);
			
		}
	
	public void enterCardPin(int cardPin){
	
		billingCardPIN.sendKeys(String.valueOf(cardPin));
	
		}

	public void enterCardPin(String cardPin){
		
		billingCardPIN.sendKeys(cardPin);
	
		}
	
	public void clickOnSubmitButton(){
		
		billingSubmitButton.click();
	
		}
	
	
	public void clickOnCancelButton(){
		
		billingCancelButton.click();
	
		}
	
public void clickOnResetButton(){
		
		billingResetButton.click();
	
		}
	
	
public ThankYouPage clickOnConfirmActionButton(){
	
	billingConfirmActionButton.click();
	return new ThankYouPage();

	}
	
	
public void clickOnBackActionButton(){
	
	billingBackActionButton.click();

	}

public void clickOnCancelActionButton(){
	
	billingCancelActionButton.click();

	}
	
	
}
