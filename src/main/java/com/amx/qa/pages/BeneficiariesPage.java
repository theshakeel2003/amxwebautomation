package com.amx.qa.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.amx.qa.base.TestBase;
import com.amx.qa.util.DropDownUtility;
import com.amx.qa.util.TestUtil;
import com.amx.qa.util.VerificationUtility;
import com.amx.qa.util.WaitUtility;
import com.aventstack.extentreports.Status;

public class BeneficiariesPage extends TestBase {
	
	
	WaitUtility waitUtility;
	DropDownUtility dropDownUtility;
	VerificationUtility verificationUtility;
	

	@FindBy(xpath = "//a[@href=\"/app/add-bene\"]")
	WebElement addBeneficiary;
	
	@FindBy(xpath = "//div[@class='content-wrapper beneficiaries fade-in']/h4[text()='Beneficiaries']")
	WebElement labelBeneficiaries;
	
	@FindBy(xpath = "//div[@class='beneficiaries-group loading-inline']")
	WebElement loaderImage;
	
	@FindBy(xpath="//label[@class='list-grid-toggle']")
	WebElement listGirdToggle;
	
	
	@FindBy(xpath = "//button[text()='Next']")
	WebElement nextButton;
	
	@FindBy(xpath="//div[@class='beneficiaries-group loading-inline']")
	WebElement loader;
	
	
	@FindBy(xpath="//div[@id='react-select-4--value']//div[@class='Select-input']/input")
	WebElement beneficiaryCountyDropDown;
	
	@FindBy(xpath="(//div[@class='Select has-value is-searchable Select--single']//div/input)[2]")
	WebElement beneficiaryStatusDropDown;
	
	@FindBy(xpath="//table[@class='table table-horizontal']/tbody/tr")
	List<WebElement> beneficiaryTableRowCount;
	
	
	@FindBy(xpath="//table[@class='table table-horizontal']/thead/tr/th")
	List<WebElement> beneficiaryTableColumnCount;
		
	String beneficiaryTableRowStart="//table[@class='table table-horizontal']/tbody/tr[";
	String beneficiaryTableColumn="]/td[";
	String beneficiaryTableRowEnd="]";
	

	@FindBy(xpath="//div[@class='pagination']/span[@class='next ']")
	WebElement paginationNext;
	
	@FindBy(xpath="//div[@class='pagination']/span[@class='pre ']")
	WebElement paginationPrevious;
	
	
	
	// Initializing the Page Objects:
	public BeneficiariesPage() {
		PageFactory.initElements(driver, this);
		waitUtility=new WaitUtility(driver);
		dropDownUtility=new DropDownUtility(driver);
		verificationUtility=new VerificationUtility(driver);

	}
	
	
public void waitForloader(WebDriver driver,int timeout) {
		
		new WebDriverWait(driver, timeout).until(ExpectedConditions.invisibilityOf(loader));

	}
	
	public CountryAndChannelPage clickOnAddBeneficiary() {

		TestUtil.waitForAnyObject(addBeneficiary);
		addBeneficiary.click();
		return new CountryAndChannelPage();
	}
		
	
public void selectBeneficiaryCountryAndStatus(String dropDownValue,String stausValues) throws InterruptedException {
		
	try {	
		waitUtility.waitForElement(loaderImage, TestUtil.IMPLICIT_WAIT);				
		dropDownUtility.selectDropDown(beneficiaryCountyDropDown, dropDownValue);
		dropDownUtility.selectDropDown(beneficiaryStatusDropDown, stausValues);
	}catch (Exception e)
	{
		logger.log(Status.FAIL, "Exception :" + e);
	}

}
	
	public void clickOnListGrid() {
		
		listGirdToggle.click();
	}
	
	public RemittanceDetails clickOnRemitNowAction(String benficiaryName,String bankName,String beneAccountNumber,String service,String currency) {
		
	int totalRowCount=beneficiaryTableRowCount.size();
	int totalColumnCount=beneficiaryTableColumnCount.size();
	ArrayList<String> list = new ArrayList<String>();
	
	boolean flag=false;
	String beneficiaryCellData=null;
	do {
		
		for(int row=1;row<=totalRowCount;row++) {
			
		  for(int column=1;column<=7;column++)
			{
			
			  String beneficiaryDetailsXpath=beneficiaryTableRowStart+row+beneficiaryTableColumn+column+beneficiaryTableRowEnd;
			  WebElement beneficiaryLocator=driver.findElement(By.xpath(beneficiaryDetailsXpath));
			  beneficiaryCellData=verificationUtility.getText(beneficiaryLocator);
			  list.add(beneficiaryCellData);
			  //System.out.println("Row Data"+beneficiaryCellData);
	
		if(list.contains(benficiaryName) && list.contains(bankName) && list.contains(beneAccountNumber) && list.contains(service) && list.contains(currency))
	
		{
		//	System.out.println("Record Found");
			String actionRemitNowEndXpath="8]/div/span[@title='Remit Now']";
			String actionRemitNowCompleteXpath=beneficiaryTableRowStart+row+beneficiaryTableColumn+actionRemitNowEndXpath;
			WebElement actionRemitNowLocator=driver.findElement(By.xpath(actionRemitNowCompleteXpath));
			actionRemitNowLocator.click();
			flag=true;
			break;
		}
	     
			}
		if(flag) {
					break;
				 }
		
		
		}
	
	if(flag==false) {
		paginationNext.click();	
	}
	
	
	}while(flag==false);
	

	return new RemittanceDetails();
	}
	

	
	
	
public void selectBeneficiaryStatus(String dropDownValue) throws InterruptedException {
			
		dropDownUtility.selectDropDown(beneficiaryStatusDropDown, dropDownValue);

}
	
	public void clickOnPaginationNext() {
		
		try {
		
		paginationNext.click();
		}catch (Exception e)
		{
			logger.log(Status.FAIL, "Exception :" + e);
		}

	}
	
public void clickOnPaginationPrevious() {
		
		try {
		paginationPrevious.click();
		}catch (Exception e)
		{
			logger.log(Status.FAIL, "Exception :" + e);
		}

	}
	
}