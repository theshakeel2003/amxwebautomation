package com.amx.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.amx.qa.base.TestBase;
import com.amx.qa.util.DropDownUtility;
import com.amx.qa.util.TestUtil;
import com.amx.qa.util.TextFieldUtility;
import com.amx.qa.util.VerificationUtility;
import com.amx.qa.util.WaitUtility;
import com.aventstack.extentreports.Status;

public class ThankYouPage extends TestBase {
	
	WaitUtility waitUtility;
	DropDownUtility dropDownUtility;
	VerificationUtility verificationUtility;
	TextFieldUtility textFieldUtility;

	@FindBy(xpath = "//button[text()='Download Transaction Reciept']")
	WebElement downLoadTransactionReciept;
	
	@FindBy(xpath = "//div[contains(text(),'Thank you for choosing Al Mulla Exchange')]")
	WebElement thankYouTextMessage;
	
	@FindBy(xpath = "//*[@class='successIco']")
	WebElement successIco;
	
	@FindBy(xpath = ".//*[@id='app']/div/div[1]/div/div/div[3]/div/div[2]/div[2]/br[1]")
	WebElement transactionAmount;
	
	
	
	// Initializing the Page Objects:
	public ThankYouPage() {
		PageFactory.initElements(driver, this);
		waitUtility=new WaitUtility(driver);
		dropDownUtility=new DropDownUtility(driver);
		verificationUtility=new VerificationUtility(driver);
		textFieldUtility=new TextFieldUtility(driver);
	}
	
	
	public void clickOnDownLoadTransactionReceipt() {
	
		try {	
			waitUtility.waitForElement(downLoadTransactionReciept, TestUtil.IMPLICIT_WAIT);
			downLoadTransactionReciept.click();
			Thread.sleep(5000);
		}catch (Exception e)
		{
			logger.log(Status.FAIL, "Exception :" + e);
		}
	}
	
	public boolean verifyDownLoadTransactionReceiptButton() {
		
		boolean downLoadTransactionRecieptButton=verificationUtility.isDisplayed(downLoadTransactionReciept);
		return downLoadTransactionRecieptButton;
	}
	
	public boolean verifyThankYouText() {
	
	boolean thankYouText=verificationUtility.isDisplayed(thankYouTextMessage);
		return thankYouText;
	}
	
	
	public boolean verifySucceassIcon() {
		
		boolean thankYouText=verificationUtility.isDisplayed(thankYouTextMessage);
			return thankYouText;
		}
	
	
	public void verifyTransactionAmount() {
		
//		String afterTransactionAmount=transactionAmount.getText();
//		String afterTransactionAmount=verificationUtility.getText(transactionAmount);
		String afterTransactionAmountinnertext=transactionAmount.getText();
		
		//System.out.println("Print text"+afterTransactionAmount);
		System.out.println("Print Inner text"+afterTransactionAmountinnertext);
	//		return afterTransactionAmount;
		}
	
	
	
	
	
	
}
