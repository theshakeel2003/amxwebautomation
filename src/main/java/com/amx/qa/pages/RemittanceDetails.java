package com.amx.qa.pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.amx.qa.base.TestBase;
import com.amx.qa.util.DropDownUtility;
import com.amx.qa.util.TestUtil;
import com.amx.qa.util.TextFieldUtility;
import com.amx.qa.util.VerificationUtility;
import com.amx.qa.util.WaitUtility;
import com.aventstack.extentreports.Status;

public class RemittanceDetails extends TestBase {
	
	WaitUtility waitUtility;
	DropDownUtility dropDownUtility;
	VerificationUtility verificationUtility;
	TextFieldUtility textFieldUtility;

	@FindBy(xpath = "//a[@href=\"/app/add-bene\"]")
	WebElement addBeneficiary;
	
	@FindBy(xpath = "//button[text()='Next']")
	WebElement nextButton;
	
	@FindBy(xpath="//div[@class='beneficiaries-group loading-inline']")
	WebElement loader;
	
	@FindBy(xpath="//label[@class='ff-label'][text()='Amount']")
	WebElement remitAmountLablel;
	
	@FindBy(xpath="//div[@class='floating-label-input  floating-label-input col dom-amount input-loading  ']/input")
	WebElement remitAmountTextField;
	
	@FindBy(xpath="//div[@class='Select has-value is-searchable Select--single']//div/input")
	WebElement sourceOfFunds;
	
	@FindBy(xpath="//div[@class='Select is-searchable Select--single']/div//input")
	WebElement purposeOfTransaction;
	
	@FindBy(xpath="//label[@for='termsCheck']")
	WebElement termsConditionCheckBox;
	
	@FindBy(xpath="//button[contains(text(),'OK')]")
	WebElement termsConditionOKButton;
	
	@FindBy(xpath="(//div[@class='info-item-2 clear']/div[@class='col rfloat'])[1]")
	WebElement kwdRate;
	
	@FindBy(xpath="(//div[@class='info-item-2 clear']/div[@class='col rfloat'])[2]")
	WebElement youPayAmount;
	
	
	@FindBy(xpath="(//div[@class='info-item-2 clear']/div[@class='col rfloat'])[3]")
	WebElement receiveAmount;
	

	// Initializing the Page Objects:
	public RemittanceDetails() {
		PageFactory.initElements(driver, this);
		waitUtility=new WaitUtility(driver);
		dropDownUtility=new DropDownUtility(driver);
		verificationUtility=new VerificationUtility(driver);
		textFieldUtility=new TextFieldUtility(driver);
	}
	
	
public void waitForloader(WebDriver driver,int timeout) {
		
		new WebDriverWait(driver, timeout).until(ExpectedConditions.invisibilityOf(loader));

	}
	
	public CountryAndChannelPage clickOnAddBeneficiary() {

		try {
		TestUtil.waitForAnyObject(addBeneficiary);
		addBeneficiary.click();
		}catch (Exception e)
		{
			logger.log(Status.FAIL, "Exception :" + e);
		}

		return new CountryAndChannelPage();
		
	}
	
	public void enterRemittanceAmount(double amountValues) throws InterruptedException {

		try {
			
			waitUtility.waitForElement(remitAmountLablel, TestUtil.IMPLICIT_WAIT);
			Actions actions = new Actions(driver);
			actions.moveToElement(remitAmountLablel).click().build().perform();
			Thread.sleep(5000);
			remitAmountTextField.sendKeys(String.valueOf(amountValues));
			}catch (Exception e)
				{
					logger.log(Status.FAIL, "Exception :" + e);
				}
	
			}
		
	public void selectSourceOfFund(String sourceOfFundsValue) throws InterruptedException {
		
		try {
		waitUtility.waitForElement(sourceOfFunds, TestUtil.IMPLICIT_WAIT);				
		dropDownUtility.selectDropDown(sourceOfFunds, sourceOfFundsValue);
		}catch (Exception e)
		{
			logger.log(Status.FAIL, "Exception :" + e);
		}


		}
	
public void selectPurposeOfTransaction(String purposeOfTransactionValue) throws InterruptedException {
		
		try {
		waitUtility.waitForElement(purposeOfTransaction, TestUtil.IMPLICIT_WAIT);				
		dropDownUtility.selectDropDown(purposeOfTransaction, purposeOfTransactionValue);
		}catch (Exception e)
		{
			logger.log(Status.FAIL, "Exception :" + e);
		}

		}
	

		public void clickOnTermsAndConditions() {
			
			try {
			termsConditionCheckBox.click();
			}catch (Exception e)
			{
				logger.log(Status.FAIL, "Exception :" + e);
			}

			
			}
		
		public void clickOnTermsAndConditionsOKButton() {
			
			try {
			
			termsConditionOKButton.click();
			}catch (Exception e)
			{
				logger.log(Status.FAIL, "Exception :" + e);
			}

		}
		
	
		public void clickOnNextButton() {
			
			try {
			nextButton.click();
			}catch (Exception e)
			{
				logger.log(Status.FAIL, "Exception :" + e);
			}

	
		}
		
		
		public String getKwdRate() {
		
			
			String remittanceKwdRate = verificationUtility.getText(kwdRate);
			return remittanceKwdRate;
		}
		
		
	public String getYouPayAmount() {
			
			 String youPayAmountValue=verificationUtility.getText(youPayAmount);
			 return youPayAmountValue;
		}
		
	public String getReceiveAmount() {
		
		 String receiveAmountValue=verificationUtility.getText(receiveAmount);
		return receiveAmountValue;
	}	
	
	
	
	}
	
	

