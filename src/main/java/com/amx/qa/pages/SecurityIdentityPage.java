package com.amx.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.amx.qa.base.TestBase;

public class SecurityIdentityPage extends TestBase {

	@FindBy(xpath = "//img[contains(@src,'https://cdnb-kwt.almullaexchange.com/onlinewebapp/dist/file')]")
	WebElement secureAccessImage;

	@FindBy(xpath = "//label[contains(text(),'Verify caption')]")
	WebElement verifyCaption;
	
	@FindBy(xpath = "//h4[contains(text(),'Security questions')]")
	WebElement securityQuestionsLabel;

	@FindBy(xpath = "//input[@id='securityQuestion']")
	WebElement securityAnswerInputText;
	
	@FindBy(xpath = "//button[contains(text(),'Next')]")
	WebElement nextButton;
	

	// Initializing the Page Objects:
	public SecurityIdentityPage() {
		PageFactory.initElements(driver, this);
	}
	
	public String verifyHomePageTitle(){
		return driver.getTitle();
	}
	
	
	public boolean verifySecureAccessImage(){
		return secureAccessImage.isDisplayed();
	}
	
	public boolean verifySecurityQuestions(){
		return securityQuestionsLabel.isDisplayed();
	}
	
	
	public void clickOnSecureAccessImage(){
		secureAccessImage.click();
	}
	
	public void clickOnVerifyCaption(){
		verifyCaption.click();
	}

	public RemittanceDetails clickOnNextButton(String securityAnswer){
		
		securityAnswerInputText.sendKeys(securityAnswer);
		nextButton.click();
		return new RemittanceDetails();

	}
	
	
	
	
	
	
	

}
