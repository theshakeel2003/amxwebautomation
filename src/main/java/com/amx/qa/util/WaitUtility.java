package com.amx.qa.util;

import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class WaitUtility{
	
	public WebDriver driver;
	
	public WaitUtility(WebDriver driver) {
		
		this.driver=driver;				
	}
	
	public void setImplicitWait(long timeout,TimeUnit unit) {
		
		driver.manage().timeouts().implicitlyWait(timeout, unit);
	}
	
	public WebDriverWait getWait(int timeOutInSeconds,int pollingEveryInMiliSec) {
		
		WebDriverWait wait= new WebDriverWait(driver,timeOutInSeconds);
		wait.pollingEvery(Duration.ofMillis(pollingEveryInMiliSec));
		wait.ignoring(NoSuchElementException.class);
		wait.ignoring(ElementNotVisibleException.class);
		wait.ignoring(StaleElementReferenceException.class);
		wait.ignoring(NoSuchFrameException.class);
		return wait;
	}
	
	public void waitForElementVisibleWithPollingTime(WebElement element,int timeOutInSeconds,int pollingEveryInMilliSeconds)
	{
		WebDriverWait wait=getWait(timeOutInSeconds, pollingEveryInMilliSeconds);
		wait.until(ExpectedConditions.visibilityOf(element));
				
	}
	
	
	public void waitForElement(WebElement element,long timeOutInSeconds)
	{
		WebDriverWait wait=	new WebDriverWait(driver,timeOutInSeconds);
		wait.until(ExpectedConditions.visibilityOf(element));
						
	}



	public boolean waitForElementNotPresent(WebElement element,long timeOutInSeconds)
	{
		WebDriverWait wait=	new WebDriverWait(driver,timeOutInSeconds);
		boolean status=wait.until(ExpectedConditions.invisibilityOf(element));
		return status;
						
	}
	

}
