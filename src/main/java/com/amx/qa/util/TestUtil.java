package com.amx.qa.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

import com.amx.qa.base.TestBase;
import com.aventstack.extentreports.Status;
import com.google.common.base.Function;

public class TestUtil extends TestBase {

	public static long PAGE_LOAD_TIMEOUT = 30;
	public static long IMPLICIT_WAIT = 30;

	public static String TESTDATA_SHEET_PATH = "/Users/naveenkhunteta/Documents/workspace"
			+ "/FreeCRMTest/src/main/java/com/crm/qa/testdata/FreeCrmTestData.xlsx";

	static Workbook book;
	static Sheet sheet;
	static JavascriptExecutor js;

	public void switchToFrame() {
		driver.switchTo().frame("mainpanel");
	}

	public static Object[][] getTestData(String sheetName) {
		FileInputStream file = null;
		try {
			file = new FileInputStream(TESTDATA_SHEET_PATH);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			book = WorkbookFactory.create(file);
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		sheet = book.getSheet(sheetName);
		Object[][] data = new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
		// System.out.println(sheet.getLastRowNum() + "--------" +
		// sheet.getRow(0).getLastCellNum());
		for (int i = 0; i < sheet.getLastRowNum(); i++) {
			for (int k = 0; k < sheet.getRow(0).getLastCellNum(); k++) {
				data[i][k] = sheet.getRow(i + 1).getCell(k).toString();
				// System.out.println(data[i][k]);
			}
		}
		return data;
	}


	
	

	public static void runTimeInfo(String messageType, String message) throws InterruptedException {
		js = (JavascriptExecutor) driver;
		// Check for jQuery on the page, add it if need be
		js.executeScript("if (!window.jQuery) {"
				+ "var jquery = document.createElement('script'); jquery.type = 'text/javascript';"
				+ "jquery.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js';"
				+ "document.getElementsByTagName('head')[0].appendChild(jquery);" + "}");
		Thread.sleep(5000);

		// Use jQuery to add jquery-growl to the page
		js.executeScript("$.getScript('https://the-internet.herokuapp.com/js/vendor/jquery.growl.js')");

		// Use jQuery to add jquery-growl styles to the page
		js.executeScript("$('head').append('<link rel=\"stylesheet\" "
				+ "href=\"https://the-internet.herokuapp.com/css/jquery.growl.css\" " + "type=\"text/css\" />');");
		Thread.sleep(5000);

		// jquery-growl w/ no frills
		js.executeScript("$.growl({ title: 'GET', message: '/' });");
//'"+color+"'"
		if (messageType.equals("error")) {
			js.executeScript("$.growl.error({ title: 'ERROR', message: '" + message + "' });");
		} else if (messageType.equals("info")) {
			js.executeScript("$.growl.notice({ title: 'Notice', message: 'your notice message goes here' });");
		} else if (messageType.equals("warning")) {
			js.executeScript("$.growl.warning({ title: 'Warning!', message: 'your warning message goes here' });");
		} else
			System.out.println("no error message");
		// jquery-growl w/ colorized output
//		js.executeScript("$.growl.error({ title: 'ERROR', message: 'your error message goes here' });");
//		js.executeScript("$.growl.notice({ title: 'Notice', message: 'your notice message goes here' });");
//		js.executeScript("$.growl.warning({ title: 'Warning!', message: 'your warning message goes here' });");
		Thread.sleep(5000);
	}



	/**
	 * Description: Method for generic wait
	 * 
	 */
	public static void wait(int millisecs) {
		try {
			Thread.sleep(millisecs);
		} catch (InterruptedException e) {
		}
	}

	/**
	 * Description: Method to wait till any object exists
	 * 
	 */
	public static WebElement waitForAnyObject(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, IMPLICIT_WAIT);
		return wait.until(ExpectedConditions.visibilityOf(element));

	}

	public static WebElement fluentWait(final By locator) {
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(180, TimeUnit.SECONDS)
				.pollingEvery(5, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);

		WebElement foo = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				return driver.findElement(locator);
			}
		});
		return foo;
	};

	/**
	 * Description: Method to click any object
	 * 
	 */
	public static WebElement clickAnyObject(WebElement e) {
		try {
			waitForAnyObject(e);
			e.click();

		} catch (NoSuchElementException ex) {
			ex.printStackTrace();
		}
		return e;
	}

	@SuppressWarnings("unused")
	public static void selectDropDownValues(WebElement dropDownWebElement, String inputValue)
			throws InterruptedException {
		boolean isDropDownValueDisplay = false;

		do {
			TestUtil.onClick(driver, dropDownWebElement, 10);
			Actions actions = new Actions(driver);
			actions.moveToElement(dropDownWebElement).perform();
			actions.click().perform();
			actions.sendKeys(Keys.CLEAR).build().perform();
			actions.sendKeys(inputValue).build().perform();
			actions.sendKeys(Keys.ENTER).build().perform();		
			
			
		//	driver.findElement(By.xpath("//div[contains(@id,'react-select') and text()='"+inputValue+"']")).click();
			
		//	isDropDownValueDisplay = driver.findElement(By.xpath("//div[contains(@id,'react-select') and text()='"+inputValue+"']")).isDisplayed();
			String value=driver.findElement(By.xpath("//span[text()='"+inputValue+"']")).getAttribute("innerHTML");
			System.out.println("Drop down value"+value);
		} while (isDropDownValueDisplay = false);	
	}

	
	
	public static void selectDropDownCurrencyValues(WebElement dropDownWebElement, String inputValue)
			throws InterruptedException {
	
//		JavascriptExecutor js = (JavascriptExecutor) driver;
	//	js.executeScript("document.getElementByXPath('//label[contains(text(),'Currency')]/following::div[1]').setAttribute('value', 'inputValue');");
		

	//	    js.executeScript("arguments[0].setAttribute('value', 'arguments[1]')", dropDownWebElement, inputValue);
		
		
		//	boolean isDropDownValueDisplay = false;

	//	do {
			TestUtil.onClick(driver, dropDownWebElement, 10);
			Actions actions = new Actions(driver);
			actions.moveToElement(dropDownWebElement).perform();
			actions.click().perform();
			
			 for (int i = 0; i < inputValue.length(); i++){
			        char c = inputValue.charAt(i);
			        String s = new StringBuilder().append(c).toString();
			        System.out.println("Drop down value"+s);
			        actions.sendKeys(s);
			    }       
			
			
			//	actions.sendKeys(Keys.CLEAR).perform();
		//	actions.sendKeys(dropDownWebElement,inputValue).perform();
			
	//		actions.sendKeys(Keys.chord(inputValue));
			//Thread.sleep(5000);
	//		actions.sendKeys(Keys.ENTER).perform();
	//		driver.findElement(By.xpath("//div[contains(@id,'react-select') and text()='"+inputValue+"']")).click();
			
		//	driver.findElement(By.xpath("//div[contains(@id,'react-select') and text()='"+inputValue+"']")).click();
			
		//	isDropDownValueDisplay=	driver.findElement(By.xpath("//div[contains(@id,'react-select') and text()='"+inputValue+"']")).isDisplayed();
		//	isDropDownValueDisplay = driver.findElement(By.xpath("//div[contains(@id,'react-select') and text()='"+inputValue+"']")).isDisplayed();
		//	String value=driver.findElement(By.xpath("//span[text()='"+inputValue+"']")).getAttribute("innerHTML");
			System.out.println("Drop down value");
	//	} while (isDropDownValueDisplay = false);	
	}

	
	@SuppressWarnings("unused")
	public static void selectBankDropDownValues(WebElement dropDownWebElement, String inputValue)
			throws InterruptedException {
		boolean isDropDownValueDisplay = false;

		do {
	
			Actions actions = new Actions(driver);
			actions.moveToElement(dropDownWebElement).perform();
			actions.click().perform();
			actions.sendKeys(inputValue).build().perform();
			driver.findElement(By.xpath("//*[@id='react-select-5--option-0']")).click();
			isDropDownValueDisplay = driver.findElement(By.xpath("//span[text()='"+inputValue+"']")).isDisplayed();
			String value=driver.findElement(By.xpath("//span[text()='"+inputValue+"']")).getAttribute("innerHTML");
			System.out.println("Drop down value"+value);
		} while (isDropDownValueDisplay = false);	
	}

	
	
	
	public static void selectAccountTypeDropDownValues(WebElement dropDownWebElement, String inputValue)
			throws InterruptedException {
		boolean isDropDownValueDisplay = false;

		do {
	
			Actions actions = new Actions(driver);
			actions.moveToElement(dropDownWebElement).perform();
			actions.click().perform();
		
			actions.sendKeys(inputValue).build().perform();
			actions.sendKeys(Keys.RETURN).build().perform();
	
			isDropDownValueDisplay = driver.findElement(By.xpath("//span[text()='"+inputValue+"']")).isDisplayed();
			String value=driver.findElement(By.xpath("//span[text()='"+inputValue+"']")).getAttribute("innerHTML");
			System.out.println("Drop down value:"+value);
		} while (isDropDownValueDisplay = false);	
	}
	
	
	
	
	
	
	
	
	public static Boolean waitForAnyObjectTillNotVisible(WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, IMPLICIT_WAIT);
		return wait.until(ExpectedConditions.invisibilityOf(element));

	}

	
	
	public static void sendKeys(WebDriver driver,WebElement element,int timeout, String inputValue) {
		new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOf(element));
		element.sendKeys(inputValue);

	}
	
	public static void onClick(WebDriver driver,WebElement element,int timeout) {
		new WebDriverWait(driver, timeout).until(ExpectedConditions.elementToBeClickable(element));
		element.click();

	}
	
	public static void waitForloader(WebDriver driver,WebElement element,int timeout) {
		
		
		new WebDriverWait(driver, timeout).until(ExpectedConditions.invisibilityOf(element));

	}
	
	//TestNG Assertion
		public static void validateFieldTextNotEmpty(String fieldValues) {
			
			SoftAssert softassert=new SoftAssert();
			softassert.assertTrue(!fieldValues.isEmpty(), "Remittance Value is Empty");
			softassert.assertAll();
		}
		

		public static void validateString(String expectedResults,String actualResults) {
					
					SoftAssert softassert=new SoftAssert();
					logger.log(Status.INFO, "Validation" );
					softassert.assertTrue(expectedResults.equalsIgnoreCase(actualResults),"Test-Case Failed-Mismatched Found");
					softassert.assertAll();
				}
		
		
		public static void validateValue(String expectedResults,String actualResults) {
			
			SoftAssert softassert=new SoftAssert();
			logger.log(Status.INFO, "Validation" );
			softassert.assertTrue(expectedResults.contains(actualResults), "Test-Case Failed-Mismatched Found");
			softassert.assertAll();
		}
	
	

	
		public static String getCurrentDateTime(){

			DateFormat customFormat=new SimpleDateFormat("MM_dd_yyyy_HH_mm_ss");
			Date currentDate=new Date();
			customFormat.format(currentDate);
			return customFormat.format(currentDate);
			}
	
	
	
	
		public static String captureScreenshot(WebDriver driver)
		
		{
		
			
			File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		String screenshotPath=System.getProperty("user.dir")+ "/screenshots/AMX_"+getCurrentDateTime() +".png";

		try{
		FileHandler.copy(src,new File(screenshotPath));
		System.out.println("Screenshot captured ");
		 }catch(IOException e)
		{
		System.out.println("Unable to capture screenshot "+e.getMessage());
		
		}
		
		return screenshotPath;
	
		}
	
	
		public static void takeScreenshotAtEndOfTest() throws IOException {
			
			
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			String currentDir = System.getProperty("user.dir");
			FileUtils.copyFile(scrFile, new File(currentDir + "/screenshots/" + System.currentTimeMillis() + ".png"));
		}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
