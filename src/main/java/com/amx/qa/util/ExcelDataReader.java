package com.amx.qa.util;

import java.util.ArrayList;

public class ExcelDataReader {
	static Xls_Reader reader;
	
	public static ArrayList<Object[]> getHeaderDataFromExcel(){
		ArrayList<Object[]> myData=new ArrayList<Object[]>();
		try {
			String path=System.getProperty("user.dir");
			reader=new Xls_Reader(path+"\\src\\main\\java\\com\\amx\\qa\\testdata\\TestInputData.xlsx");
					
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		for(int rowNum=2; rowNum<=reader.getRowCount("HeaderMetaInfo");rowNum++) {
			String countryId=reader.getCellData("HeaderMetaInfo","countryId", rowNum);
			String customerId=reader.getCellData("HeaderMetaInfo","customerId", rowNum);
			String companyId=reader.getCellData("HeaderMetaInfo","companyId", rowNum);
			String channel=reader.getCellData("HeaderMetaInfo","channel", rowNum);
			String countryBranchId=reader.getCellData("HeaderMetaInfo","countryBranchId", rowNum);
			String tenant=reader.getCellData("HeaderMetaInfo","tenant", rowNum);
			String languageId=reader.getCellData("HeaderMetaInfo","languageId", rowNum);
			String employeeId=reader.getCellData("HeaderMetaInfo","employeeId", rowNum);
			String testcondition=reader.getCellData("HeaderMetaInfo","testcondition", rowNum);
			
			Object headerMapArray[]= {countryId,customerId,companyId,channel,countryBranchId,tenant,languageId,employeeId,testcondition};
			myData.add(headerMapArray);

		}
		return myData;
		}
	
	
		
		
	}
	


