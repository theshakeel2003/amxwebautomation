package com.amx.qa.util;

import java.math.BigDecimal;
import java.util.ArrayList;

public class ExcelDataReaderRemittance {
	static Xls_Reader reader;

	public static ArrayList<Object[]> getBeneficiaryDetailsForRemittanceFromExcel(String excelPath) {
		ArrayList<Object[]> excelDataList = new ArrayList<Object[]>();
		try {

			reader = new Xls_Reader(excelPath);

		} catch (Exception e) {
			e.printStackTrace();
		}

		for (int rowNum = 2; rowNum <= reader.getRowCount("BeneRemittancePricing"); rowNum++) {
			
			String runMode = reader.getCellData("BeneRemittancePricing", "RunMode", rowNum);
			String beneficiaryCountry = reader.getCellData("BeneRemittancePricing", "BeneficiaryCountry", rowNum);
			String beneficiaryStatus = reader.getCellData("BeneRemittancePricing", "BeneficiaryStatus", rowNum);
			
			String beneficiaryName = reader.getCellData("BeneRemittancePricing", "BeneficiaryName", rowNum);
			String bankName = reader.getCellData("BeneRemittancePricing", "BankName", rowNum);
			String beneficiaryAccountNumber = reader.getCellData("BeneRemittancePricing", "BeneficiaryAccountNumber", rowNum);
			String service = reader.getCellData("BeneRemittancePricing", "Service", rowNum);
			String beneficiaryCurrency = reader.getCellData("BeneRemittancePricing", "BeneficiaryCurrency", rowNum);
			String securityAnswer = reader.getCellData("BeneRemittancePricing", "securityAnswer", rowNum);
			String remittanceAmount = reader.getCellData("BeneRemittancePricing", "RemittanceAmount", rowNum);
			String sourceOfFunds = reader.getCellData("BeneRemittancePricing", "SourceOfFunds", rowNum);
			String purposeOfTransaction = reader.getCellData("BeneRemittancePricing", "PurposeOfTransaction", rowNum);
	
	
			Object excelDataMapArray[] = {runMode,beneficiaryCountry,beneficiaryStatus,beneficiaryName,bankName,beneficiaryAccountNumber,service,beneficiaryCurrency,securityAnswer,remittanceAmount,
					sourceOfFunds,purposeOfTransaction};
			excelDataList.add(excelDataMapArray);
			
		}
		return excelDataList;
	}

	
	
	
	
	
	public static ArrayList<Object[]> getBeneficiaryDetailsWithKnetFromExcel(String excelPath) {
		ArrayList<Object[]> excelDataList = new ArrayList<Object[]>();
		try {

			reader = new Xls_Reader(excelPath);

		} catch (Exception e) {
			e.printStackTrace();
		}

		for (int rowNum = 2; rowNum <= reader.getRowCount("BeneRemittancePricingWithKnet"); rowNum++) {
			
			String runMode = reader.getCellData("BeneRemittancePricingWithKnet", "RunMode", rowNum);
			String beneficiaryCountry = reader.getCellData("BeneRemittancePricingWithKnet", "BeneficiaryCountry", rowNum);
			String beneficiaryStatus = reader.getCellData("BeneRemittancePricingWithKnet", "BeneficiaryStatus", rowNum);
			
			String beneficiaryName = reader.getCellData("BeneRemittancePricingWithKnet", "BeneficiaryName", rowNum);
			String bankName = reader.getCellData("BeneRemittancePricingWithKnet", "BankName", rowNum);
			String beneficiaryAccountNumber = reader.getCellData("BeneRemittancePricingWithKnet", "BeneficiaryAccountNumber", rowNum);
			String service = reader.getCellData("BeneRemittancePricingWithKnet", "Service", rowNum);
			String beneficiaryCurrency = reader.getCellData("BeneRemittancePricingWithKnet", "BeneficiaryCurrency", rowNum);
			String securityAnswer = reader.getCellData("BeneRemittancePricingWithKnet", "securityAnswer", rowNum);
			String remittanceAmount = reader.getCellData("BeneRemittancePricingWithKnet", "RemittanceAmount", rowNum);
			String sourceOfFunds = reader.getCellData("BeneRemittancePricingWithKnet", "SourceOfFunds", rowNum);
			String purposeOfTransaction = reader.getCellData("BeneRemittancePricingWithKnet", "PurposeOfTransaction", rowNum);
	//		String KNetPaymentStatus = reader.getCellData("BeneRemittancePricingWithKnet", "KNetPayment", rowNum);
			String paymentBankName = reader.getCellData("BeneRemittancePricingWithKnet", "PaymentBankName", rowNum);
			String paymentCardNumber = reader.getCellData("BeneRemittancePricingWithKnet", "PaymentCardNumber", rowNum);
			String paymentCardExpiryMonth = reader.getCellData("BeneRemittancePricingWithKnet", "PaymentCardExpiryMonth", rowNum);
			String paymentCardExpiryYear = reader.getCellData("BeneRemittancePricingWithKnet", "PaymentCardExpiryYear", rowNum);
			String paymentCardPIN = reader.getCellData("BeneRemittancePricingWithKnet", "PaymentCardPIN", rowNum);
			
			Object excelDataMapArray[] = {runMode,beneficiaryCountry,beneficiaryStatus,beneficiaryName,bankName,beneficiaryAccountNumber,service,beneficiaryCurrency,securityAnswer,remittanceAmount,
					sourceOfFunds,purposeOfTransaction,paymentBankName,paymentCardNumber,paymentCardExpiryMonth,paymentCardExpiryYear,paymentCardPIN};
			excelDataList.add(excelDataMapArray);
			
		}
		return excelDataList;
	}
	
	
	
	
															

	
	
	
	
	

}
