package com.amx.qa.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import com.amx.qa.util.TestUtil;
import com.amx.qa.util.WebEventListener;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestBase {

	public static WebDriver driver;
	public static Properties prop;
	public static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	public static TestUtil testUtil;
	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extentReport;
	public static ExtentTest logger;
	//public static String receiptPath = System.getProperty("user.dir") + "/TransactionReceipt";

	@BeforeSuite
	public void setUpSuite() {

		
//		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/test-output/AMX-AutomationTestResults_"+TestUtil.getCurrentDateTime() + ".html");
		
		htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/test-output/AMX-AutomationTestResults.html");
		extentReport = new ExtentReports();
		extentReport.attachReporter(htmlReporter);
		extentReport.setSystemInfo("Host Name", "AMX");
		extentReport.setSystemInfo("Environment", "APPB/QA");
		extentReport.setSystemInfo("User Name", "Shakeel");

	}

	public static void initialization() {

		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream(
					System.getProperty("user.dir") + "/src/main/java/com/amx" + "/qa/config/config.properties");

			prop.load(ip);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

/*
		String OS = System.getProperty("os.name");
		if (OS.toUpperCase().contains("WINDOWS"))

		{
			String browserName = prop.getProperty("browser");
			if (browserName.toUpperCase().contains("CHROME")) {
				WebDriverManager.chromedriver().setup();
				// System.setProperty("webdriver.chrome.driver",
				// "D:/Repos/amx-qa/com.amxremit.UIAutomation/src/main/java/com/amx/qa/drivers/chromedriver.exe");
			
				String receiptPath = System.getProperty("user.dir") + "\\TransactionReceipt";
		
				ChromeOptions options = new ChromeOptions();
				Map<String, Object> prefs = new HashMap<String, Object>();
				prefs.put("profile.default_content_setting.popups", 0);
				prefs.put("download.default_directory", receiptPath);
				options.setExperimentalOption("prefs", prefs);
				DesiredCapabilities cap = DesiredCapabilities.chrome();
				cap.setCapability(ChromeOptions.CAPABILITY, options);
//				logger.log(Status.INFO, "Chrome Browser Started");
				driver = new ChromeDriver(cap);
				
			} else if (browserName.toUpperCase().contains("FF") || browserName.toUpperCase().contains("FIREFOX")) {
				// System.setProperty("webdriver.gecko.driver",
				// "/Users/naveenkhunteta/Documents/SeleniumServer/geckodriver");
				
				String receiptPath = System.getProperty("user.dir") + "\\TransactionReceipt";
				
				WebDriverManager.firefoxdriver().setup();
				FirefoxProfile options = new FirefoxProfile();

				Map<String, Object> prefs = new HashMap<String, Object>();
				prefs.put("profile.default_content_setting.popups", 0);
				prefs.put("download.default_directory", receiptPath);

				options.setPreference("profile.default_content_setting.popups", 0);
				options.setPreference("download.default_directory", receiptPath);

				DesiredCapabilities cap = DesiredCapabilities.firefox();
				cap.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);

	//			logger.log(Status.INFO, "FireFox Browser Started");
				driver = new FirefoxDriver(cap);
			}
		}

		else if (OS.toUpperCase().contains("MAC")) {

			String browserName = prop.getProperty("browser");
			if (browserName.toUpperCase().contains("CHROME")) {
				WebDriverManager.chromedriver().setup();
				// System.setProperty("webdriver.chrome.driver","D:/Repos/amx-qa/com.amxremit.UIAutomation/src/main/java/com/amx/qa/drivers/chromedriver.exe");
				
				String receiptPath = System.getProperty("user.dir") + "\\TransactionReceipt";
				
				ChromeOptions options = new ChromeOptions();
				Map<String, Object> prefs = new HashMap<String, Object>();
				prefs.put("profile.default_content_setting.popups", 0);
				prefs.put("download.default_directory", receiptPath);
				options.setExperimentalOption("prefs", prefs);
				DesiredCapabilities cap = DesiredCapabilities.chrome();
				cap.setCapability(ChromeOptions.CAPABILITY, options);
				logger.log(Status.INFO, "Chrome Browser Started");
				driver = new ChromeDriver(cap);

			} else if (browserName.toUpperCase().contains("FF") || browserName.toUpperCase().contains("FIREFOX")) {
				// System.setProperty("webdriver.gecko.driver",
				// "/Users/naveenkhunteta/Documents/SeleniumServer/geckodriver");
			
				String receiptPath = System.getProperty("user.dir") + "\\TransactionReceipt";
				
				WebDriverManager.firefoxdriver().setup();
				FirefoxProfile options = new FirefoxProfile();

				Map<String, Object> prefs = new HashMap<String, Object>();
				prefs.put("profile.default_content_setting.popups", 0);
				prefs.put("download.default_directory", receiptPath);

				options.setPreference("profile.default_content_setting.popups", 0);
				options.setPreference("download.default_directory", receiptPath);

				DesiredCapabilities cap = DesiredCapabilities.firefox();
				cap.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);

				logger.log(Status.INFO, "FireFox Browser Started");
				driver = new FirefoxDriver(cap);
			}

		}

		e_driver = new EventFiringWebDriver(driver);
		// Now create object of EventListerHandler to register it with
		// EventFiringWebDriver
		eventListener = new WebEventListener();
		e_driver.register(eventListener);
		driver = e_driver;
	//	driver.manage().window().maximize();
	//	driver.manage().deleteAllCookies();
	//	driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
	//	driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
	//	driver.get(prop.getProperty("url"));

*/
	}
	
	
	public static void openBrowser() {
		
	
		String OS = System.getProperty("os.name");
		if (OS.toUpperCase().contains("WINDOWS"))

		{
			String browserName = prop.getProperty("browser");
			if (browserName.toUpperCase().contains("CHROME")) {
				WebDriverManager.chromedriver().setup();
				// System.setProperty("webdriver.chrome.driver",
				// "D:/Repos/amx-qa/com.amxremit.UIAutomation/src/main/java/com/amx/qa/drivers/chromedriver.exe");
			
				String receiptPath = System.getProperty("user.dir") + "\\TransactionReceipt";
		
				ChromeOptions options = new ChromeOptions();
				Map<String, Object> prefs = new HashMap<String, Object>();
				prefs.put("profile.default_content_setting.popups", 0);
				prefs.put("download.default_directory", receiptPath);
				options.setExperimentalOption("prefs", prefs);
				DesiredCapabilities cap = DesiredCapabilities.chrome();
				cap.setCapability(ChromeOptions.CAPABILITY, options);
//				logger.log(Status.INFO, "Chrome Browser Started");
				driver = new ChromeDriver(cap);
				
			} else if (browserName.toUpperCase().contains("FF") || browserName.toUpperCase().contains("FIREFOX")) {
				// System.setProperty("webdriver.gecko.driver",
				// "/Users/naveenkhunteta/Documents/SeleniumServer/geckodriver");
				
				String receiptPath = System.getProperty("user.dir") + "\\TransactionReceipt";
				
				WebDriverManager.firefoxdriver().setup();
				FirefoxProfile options = new FirefoxProfile();

				Map<String, Object> prefs = new HashMap<String, Object>();
				prefs.put("profile.default_content_setting.popups", 0);
				prefs.put("download.default_directory", receiptPath);

				options.setPreference("profile.default_content_setting.popups", 0);
				options.setPreference("download.default_directory", receiptPath);

				DesiredCapabilities cap = DesiredCapabilities.firefox();
				cap.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);

	//			logger.log(Status.INFO, "FireFox Browser Started");
				driver = new FirefoxDriver(cap);
			}
		}

		else if (OS.toUpperCase().contains("MAC")) {

			String browserName = prop.getProperty("browser");
			if (browserName.toUpperCase().contains("CHROME")) {
				WebDriverManager.chromedriver().setup();
				// System.setProperty("webdriver.chrome.driver","D:/Repos/amx-qa/com.amxremit.UIAutomation/src/main/java/com/amx/qa/drivers/chromedriver.exe");
				
				String receiptPath = System.getProperty("user.dir") + "\\TransactionReceipt";
				
				ChromeOptions options = new ChromeOptions();
				Map<String, Object> prefs = new HashMap<String, Object>();
				prefs.put("profile.default_content_setting.popups", 0);
				prefs.put("download.default_directory", receiptPath);
				options.setExperimentalOption("prefs", prefs);
				DesiredCapabilities cap = DesiredCapabilities.chrome();
				cap.setCapability(ChromeOptions.CAPABILITY, options);
				logger.log(Status.INFO, "Chrome Browser Started");
				driver = new ChromeDriver(cap);

			} else if (browserName.toUpperCase().contains("FF") || browserName.toUpperCase().contains("FIREFOX")) {
				// System.setProperty("webdriver.gecko.driver",
				// "/Users/naveenkhunteta/Documents/SeleniumServer/geckodriver");
			
				String receiptPath = System.getProperty("user.dir") + "\\TransactionReceipt";
				
				WebDriverManager.firefoxdriver().setup();
				FirefoxProfile options = new FirefoxProfile();

				Map<String, Object> prefs = new HashMap<String, Object>();
				prefs.put("profile.default_content_setting.popups", 0);
				prefs.put("download.default_directory", receiptPath);

				options.setPreference("profile.default_content_setting.popups", 0);
				options.setPreference("download.default_directory", receiptPath);

				DesiredCapabilities cap = DesiredCapabilities.firefox();
				cap.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);

				logger.log(Status.INFO, "FireFox Browser Started");
				driver = new FirefoxDriver(cap);
			}

		}

		e_driver = new EventFiringWebDriver(driver);
		// Now create object of EventListerHandler to register it with
		// EventFiringWebDriver
		eventListener = new WebEventListener();
		e_driver.register(eventListener);
		driver = e_driver;
	//	driver.manage().window().maximize();
	//	driver.manage().deleteAllCookies();
	//	driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
	//	driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
	//	driver.get(prop.getProperty("url"));

		
		
		
		
		
		
		
	//	driver = new ChromeDriver(cap);
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
		driver.get(prop.getProperty("url"));
	}
	
	
	
	

	public Properties getConfigDetails(String moduleConfigFile) throws IOException {

		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream(System.getProperty("user.dir") + moduleConfigFile);
			prop.load(ip);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return prop;

	}

	@AfterMethod
	public void getResults(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {

			logger.fail(MarkupHelper.createLabel(result.getName() + " Test Case Failed", ExtentColor.RED));
			// logger.fail(result.getThrowable());
			 logger.fail(result.getThrowable(),MediaEntityBuilder.createScreenCaptureFromPath(TestUtil.captureScreenshot(driver)).build());

			logger.log(Status.ERROR, "TEST CASE FAILED IS "+result.getName() );
			logger.log(Status.ERROR, "TEST CASE FAILED IS "+result.getThrowable() );
		
		}

		if (result.getStatus() == ITestResult.SUCCESS) {

			logger.pass(MarkupHelper.createLabel(result.getName() + " Test Case Passed", ExtentColor.GREEN));
			// logger.pass("Test
			// Pass",(MediaEntityBuilder.createScreenCaptureFromPath(TestUtil.captureScreenshot(driver)).build()));

		}

		if (result.getStatus() == ITestResult.SKIP) {
			logger.skip(MarkupHelper.createLabel(result.getName() + " Test Case Skiped", ExtentColor.YELLOW));

		}

		extentReport.flush();
	try
	{
		driver.quit();
	}catch(Exception e)
	{
		logger.fail(MarkupHelper.createLabel(result.getName() + " Test Case Failed", ExtentColor.RED));
	}
	
	}

	/*
	 * @AfterSuite public void endReport() { extent.flush(); }
	 */

}
