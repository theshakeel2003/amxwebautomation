/*
 * @author Shakeel Shaikh
 * 
 */

package com.amx.qa.remittance;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.amx.qa.base.TestBase;
import com.amx.qa.pages.BeneficiariesPage;
import com.amx.qa.pages.BillingInformation;
import com.amx.qa.pages.DashBoardPage;
import com.amx.qa.pages.IdentityVerification;
import com.amx.qa.pages.LoginPage;
import com.amx.qa.pages.RemittanceDetails;
import com.amx.qa.pages.SecurityIdentityPage;
import com.amx.qa.pages.ThankYouPage;
import com.amx.qa.util.AssertionHelper;
import com.amx.qa.util.ExcelDataReaderRemittance;
import com.amx.qa.util.TestUtil;
import com.amx.qa.util.TextFieldUtility;
import com.aventstack.extentreports.Status;

public class RemittanceDetailsTest extends TestBase{

	LoginPage loginPage;
	SecurityIdentityPage securityIdentityPage;
	DashBoardPage dashBoardPage; 
	BeneficiariesPage beneficiariesPage;
	RemittanceDetails remittanceDetails;
	TextFieldUtility textFieldUtility;
	BillingInformation billingInformation;
	ThankYouPage thankYouPage;
	IdentityVerification identityVerification;
	String inputDataPath;
	TestBase testInputFile;


	//test cases should be separated -- independent with each other
	//before each test case -- launch the browser and login
	//@test -- execute test case
	//after each test case -- close the browser
	
	
	@BeforeMethod
	public void setUp() {
		
		initialization();
		loginPage = new LoginPage();
		beneficiariesPage=new BeneficiariesPage();
		billingInformation=new BillingInformation();
		thankYouPage=new ThankYouPage();
		identityVerification=new IdentityVerification();
	//	dashBoardPage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
	//	dashBoardPage.clickOnBeneficiaryLink();
	}


	
	
	@DataProvider
	public Iterator<Object[]> getRemittanceTestData() throws IOException {

	//	TestBase testInputFile = new TestBase();
		testInputFile = new TestBase();
		inputDataPath = testInputFile
				.getConfigDetails("/src/main/java/com/amx/qa/config/config.properties")
				.getProperty("InputDataPath");
		ArrayList<Object[]> fetchDataFromExcel = ExcelDataReaderRemittance.getBeneficiaryDetailsForRemittanceFromExcel(inputDataPath);
		return fetchDataFromExcel.iterator();

	}
	
	@Test(priority=1,enabled = false,dataProvider = "getRemittanceTestData")
	public void verifyRemittancePricingTest(String runMode,String beneficiaryCountry,String beneficiaryStatus,String beneficiaryName,String bankName,String beneficiaryAccountNumber,String service,String beneficiaryCurrency,String securityAnswer,String remittanceAmount,
			String sourceOfFunds,String purposeOfTransaction) throws InterruptedException{
		
		
		logger = extentReport.createTest("Check Remittance Pricing from Beneficiary listing page"," Check Remittance Pricing for Beneficiary Country- " +beneficiaryCountry +" and Currency-"+beneficiaryCurrency).assignCategory("Remittance Pricing");
		if(runMode.equalsIgnoreCase("YES")) {
		beneficiariesPage.selectBeneficiaryCountryAndStatus(beneficiaryCountry,beneficiaryStatus);
		logger.log(Status.INFO, "Beneficiary Country "+beneficiaryCountry +" Selected from drop down");
	
		openBrowser();
		dashBoardPage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		dashBoardPage.clickOnBeneficiaryLink();	
		beneficiariesPage.clickOnListGrid();	
		
		remittanceDetails=beneficiariesPage.clickOnRemitNowAction(beneficiaryName,bankName,beneficiaryAccountNumber,service,beneficiaryCurrency);
		logger.log(Status.INFO, "Beneficiary Found for Remittance and Click on Remit Action");
	
		identityVerification.enterSecurityAnswer(securityAnswer);
		logger.log(Status.INFO, "Entered Security Answer");
		
		remittanceDetails=identityVerification.clickOnNextButton();
		logger.log(Status.INFO, "Clicked on Next Button");
		
		remittanceDetails.enterRemittanceAmount(Double.parseDouble(remittanceAmount));
		logger.log(Status.INFO, "Entered Remittance Amount: "+remittanceAmount);
		
		remittanceDetails.selectSourceOfFund(sourceOfFunds);
		logger.log(Status.INFO, "Source of Funds Selected as- "+sourceOfFunds);
		
		remittanceDetails.selectPurposeOfTransaction(purposeOfTransaction);
		logger.log(Status.INFO, "Purpose of Transaction Selected as- "+purposeOfTransaction);
		
		remittanceDetails.clickOnTermsAndConditions();
		remittanceDetails.clickOnTermsAndConditionsOKButton();
		logger.log(Status.INFO, "Terms and Conditions selected and Clicked on OK Button.");
		
		String kwdRate=remittanceDetails.getKwdRate();
		String youPayAmount=remittanceDetails.getYouPayAmount();
		String receiveAmount=remittanceDetails.getReceiveAmount();
		TestUtil.validateFieldTextNotEmpty(kwdRate);
		TestUtil.validateFieldTextNotEmpty(youPayAmount);
		TestUtil.validateFieldTextNotEmpty(receiveAmount);
		}
		else
		{
			logger.log(Status.SKIP, "Test case Run-Mode marked as NO");
		}
		
	}	
	
	
	
	@DataProvider
	public Iterator<Object[]> getRemittanceWithKnetTestData() throws IOException {

		TestBase testInputFile = new TestBase();
		inputDataPath = testInputFile
				.getConfigDetails("/src/main/java/com/amx/qa/config/config.properties")
				.getProperty("InputDataPath");
		ArrayList<Object[]> fetchDataFromExcel = ExcelDataReaderRemittance.getBeneficiaryDetailsWithKnetFromExcel(inputDataPath);
		return fetchDataFromExcel.iterator();

	}
	
	@Test(priority=2,enabled = false,dataProvider = "getRemittanceWithKnetTestData")
	public void verifyRemittancePricingWithKnetTest(String runMode,String beneficiaryCountry,String beneficiaryStatus,String beneficiaryName,String bankName,String beneficiaryAccountNumber,String service,String beneficiaryCurrency,String securityAnswer,String remittanceAmount,
			String sourceOfFunds,String purposeOfTransaction,String paymentBankName,String paymentCardNumber,String paymentCardExpiryMonth,String paymentCardExpiryYear,String paymentCardPIN) throws InterruptedException{
		
		logger = extentReport.createTest("Check Remittance Pricing from Beneficiary listing page"," Check Remittance Pricing for Beneficiary Country- " +beneficiaryCountry +" and Currency-"+beneficiaryCurrency).assignCategory("Remittance Pricing");
		
		if(runMode.equalsIgnoreCase("YES")) {
		beneficiariesPage.selectBeneficiaryCountryAndStatus(beneficiaryCountry,beneficiaryStatus);
		logger.log(Status.INFO, "Beneficiary Country "+beneficiaryCountry +" Selected from drop down");
		
		openBrowser();
		dashBoardPage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		dashBoardPage.clickOnBeneficiaryLink();	
		beneficiariesPage.clickOnListGrid();	
		remittanceDetails=beneficiariesPage.clickOnRemitNowAction(beneficiaryName,bankName,beneficiaryAccountNumber,service,beneficiaryCurrency);
		logger.log(Status.INFO, "Beneficiary Found for Remittance and Click on Remit Action");
	
		identityVerification.enterSecurityAnswer(securityAnswer);
		logger.log(Status.INFO, "Entered Security Answer");
		
		remittanceDetails=identityVerification.clickOnNextButton();
		logger.log(Status.INFO, "Clicked on Next Button");
		
		remittanceDetails.enterRemittanceAmount(Double.parseDouble(remittanceAmount));
		logger.log(Status.INFO, "Entered Remittance Amount: "+remittanceAmount);
		
		remittanceDetails.selectSourceOfFund(sourceOfFunds);
		logger.log(Status.INFO, "Source of Funds Selected as: "+sourceOfFunds);
		
		remittanceDetails.selectPurposeOfTransaction(purposeOfTransaction);
		logger.log(Status.INFO, "Purpose of Transaction Selected as: "+purposeOfTransaction);
		
		remittanceDetails.clickOnTermsAndConditions();
		remittanceDetails.clickOnTermsAndConditionsOKButton();
		logger.log(Status.INFO, "Terms and Conditions selected and Clicked on OK Button: ");
		
		String kwdRate=remittanceDetails.getKwdRate();
		String youPayAmount=remittanceDetails.getYouPayAmount();
		System.out.println("You Pay:"+youPayAmount);
		String receiveAmount=remittanceDetails.getReceiveAmount();
		TestUtil.validateFieldTextNotEmpty(kwdRate);
		TestUtil.validateFieldTextNotEmpty(youPayAmount);
		TestUtil.validateFieldTextNotEmpty(receiveAmount);
		remittanceDetails.clickOnNextButton();
	//	Thread.sleep(5000);
		
		billingInformation.selectBankName(paymentBankName);
		billingInformation.enterCardNumber(paymentCardNumber);
		billingInformation.selectCardExpiryMonthAndYear(paymentCardExpiryMonth, paymentCardExpiryYear);
		billingInformation.enterCardPin(paymentCardPIN);
		billingInformation.clickOnSubmitButton();
		logger.log(Status.INFO, "Entered Knet-Details ");
		thankYouPage=billingInformation.clickOnConfirmActionButton();
		boolean actualtransactionReceiptText=thankYouPage.verifyThankYouText();
		AssertionHelper.verifyTrue(actualtransactionReceiptText, "Thank You Message Not Displayed.");
		
		boolean isDownLoadTransactionReceiptDisplay=thankYouPage.verifyDownLoadTransactionReceiptButton();
		AssertionHelper.verifyTrue(isDownLoadTransactionReceiptDisplay, "DownLoad Transaction Receipt Button Not Displayed.");
		
		boolean isVerifySucceassIconDisplay=thankYouPage.verifySucceassIcon();
		AssertionHelper.verifyTrue(isVerifySucceassIconDisplay, "Transaction could not be completed.");
		thankYouPage.verifyTransactionAmount();		
		thankYouPage.clickOnDownLoadTransactionReceipt();	
		logger.log(Status.INFO, "Clicked on DownLoad Transaction Receipt.");
		
	
		}
		else
		{
			logger.log(Status.SKIP, "Test case Run-Mode marked as NO");
		}
		
	}	
	
	/*
	
	@Test(priority=3)
	public void verifyTransactionReceiptTest() throws InterruptedException, IOException{
		
		logger = extentReport.createTest("Check Transaction Receipt","Validate Receipt").assignCategory("Remittance Transaction Receipt");
	
		testInputFile = new TestBase();
		openBrowser();		
		String actualfile = System.getProperty("user.dir")+"\\TransactionReceipt\\ActualTransactionReceipt\\TransactionReceipt_jasper.pdf";
		
		String expectedfile = System.getProperty("user.dir")+"\\TransactionReceipt\\ExpectedTransactionReceipt\\TransactionReceipt_jasper.pdf";		
		
	//	String actualfile = "D:\\Repos\\amx-qa\\com.amxremit.UIAutomation\\TransactionReceipt\\TransactionReceipt_jasper.pdf";
	//	String expectedfile = "D:\\Repos\\amx-qa\\com.amxremit.UIAutomation\\TransactionReceipt\\ExpectedTransactionReceipt\\TransactionReceipt_jasper.pdf";
		
		String resultFile = System.getProperty("user.dir") + "\\TransactionReceipt\\ResultFile";
		String ignoreFile = System.getProperty("user.dir") + "\\TransactionReceipt\\ResultFile\\ignore.conf";
		
		//PDF to TXT convertor for pixel
		
		boolean isEqual=new PdfComparator(expectedfile,actualfile).withIgnore(ignoreFile).compare().writeTo(resultFile);
		
	//	boolean isEqual=new PdfComparator(expectedfile,actualfile).compare().writeTo(resultFile);
		System.out.println("Results" +isEqual);
		
	}	
	
	*/
	
		
	
	@AfterMethod
	public void tearDown(ITestResult result){
		
		if(result.getStatus()==ITestResult.FAILURE) {
			
			logger.log(Status.ERROR, "TEST CASE FAILED IS "+result.getName() );
			logger.log(Status.ERROR, "TEST CASE FAILED IS "+result.getThrowable() );
			
			
		}
				
			driver.quit();
		
	
	}
	
	
}
