package com.amx.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.amx.qa.base.TestBase;
import com.amx.qa.pages.SecurityIdentityPage;
import com.amx.qa.pages.SignUpPage;
import com.amx.qa.pages.ForgotPassword;
import com.amx.qa.pages.LoginPage;

public class LoginPageTest extends TestBase{
	LoginPage loginPage;
	SecurityIdentityPage securityIdentityPage;
	SignUpPage signupPage;
	ForgotPassword forgotPassword;
	
	public LoginPageTest(){
		super();
	}
	
	@BeforeMethod
	public void setUp(){
		initialization();
		loginPage = new LoginPage();	
	}
	
	/*
	@Test(priority=1)
	public void loginPageTitleTest(){
		String title = loginPage.validateLoginPageTitle();

		Assert.assertEquals(title, "Al Mulla Exchange");
		
	}
	
	@Test(priority=2)
	public void loginPageSignUpButtonTest(){
		
		signupPage=loginPage.clickOnSignUpButton();
		Assert.assertTrue(signupPage.validateSignUpLabel());
		
	}
	
	
	@Test(priority=3)
	public void loginPageForgotPassowrdButtonTest(){
		
		forgotPassword=loginPage.clickOnForgotPasswordButton();
		Assert.assertTrue(forgotPassword.validateVerifyOTPLabel());
		
	}
	*/
	
	@Test(priority=4)
	public void loginTest(){
		SoftAssert softassert=new SoftAssert();
	//	securityIdentityPage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		softassert.assertTrue(securityIdentityPage.verifySecureAccessImage(),"Secure Access Image Not Found.");
		softassert.assertTrue(securityIdentityPage.verifySecurityQuestions(),"Security Questions Not Found.");
		securityIdentityPage.clickOnSecureAccessImage();
		
		
		
		softassert.assertAll();
	}
	
	
	
	
	
	//@AfterMethod
	//public void tearDown(){
	//	driver.quit();
	//}
	
	
	
	

}
