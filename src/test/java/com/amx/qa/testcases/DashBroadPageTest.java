/*
 * @author Shakeel Shaikh
 * 
 */

package com.amx.qa.testcases;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.amx.qa.base.TestBase;

import com.amx.qa.pages.DashBoardPage;
import com.amx.qa.pages.SecurityIdentityPage;
import com.amx.qa.pages.LoginPage;

public class DashBroadPageTest extends TestBase{

	LoginPage loginPage;
	SecurityIdentityPage securityIdentityPage;
	DashBoardPage dashBoardPage;

	//test cases should be separated -- independent with each other
	//before each test case -- launch the browser and login
	//@test -- execute test case
	//after each test case -- close the browser
	
	
	public DashBroadPageTest(){
			super();
			
	}
	
	
	@BeforeMethod
	public void setUp() {
		
		initialization();
		loginPage = new LoginPage();
//		securityIdentityPage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		securityIdentityPage.clickOnSecureAccessImage();
	//	dashBoardPage=securityIdentityPage.clickOnNextButton("test");
		
		
	}
	
	@Test(priority=1)
	public void verifyDashBoardLabelTest(){
		SoftAssert softassert=new SoftAssert();
		softassert.assertTrue(dashBoardPage.verifyRemittenceDetailsLink(), "Remittance Page label is missing on the page");
		softassert.assertTrue(dashBoardPage.verifyMyTransactionLink(), "MyTransaction Page label is missing on the page");
		softassert.assertTrue(dashBoardPage.verifyViewBeneficiaryLink(), "View Beneficiary Page label is missing on the page");
		softassert.assertTrue(dashBoardPage.verifyViewExchangeRateLink(), "Exchange Page label is missing on the page");
	}
	
	@Test(priority=2)
	public void verifyViewBeneficiaryTest(){
		dashBoardPage.clickOnBeneficiaryLink();
		
	}	
	
/*
	@AfterMethod
	public void tearDown(){
	driver.quit();
	}
	
	*/
	
	
}
