package com.amx.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.amx.qa.base.TestBase;
import com.amx.qa.pages.DashBoardPage;
import com.amx.qa.pages.SecurityIdentityPage;
import com.amx.qa.pages.LoginPage;
import com.amx.qa.util.TestUtil;

public class SecurityIdentityPageTest extends TestBase {
	LoginPage loginPage;
	SecurityIdentityPage securityIdentityPage;
	TestUtil testUtil;
	DashBoardPage dashBoardPage;

	public SecurityIdentityPageTest() {
		super();
	}

	//test cases should be separated -- independent with each other
	//before each test case -- launch the browser and login
	//@test -- execute test case
	//after each test case -- close the browser
	
	@BeforeMethod
	public void setUp() {
		initialization();
		testUtil = new TestUtil();
		loginPage = new LoginPage();
//		securityIdentityPage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		securityIdentityPage.clickOnSecureAccessImage();
	}
	
	
	@Test(priority=1)
	public void verifyLastLoginTest() throws InterruptedException{
	//	dashBoardPage=securityIdentityPage.clickOnNextButton("test");
		String actualLastLogintext=dashBoardPage.verifyLastLoginText();
		System.out.println("Last Visited Time:"+actualLastLogintext);
		boolean lastLoginText=actualLastLogintext.contains("Your last visit was on");
		Assert.assertTrue(lastLoginText, "Your last visit time not displayed");
	
	}
	
	@Test(priority=2)
	public void verifyUserNameTest() {
	//	dashBoardPage=securityIdentityPage.clickOnNextButton("test");

		String actualUserNametext=dashBoardPage.verifyUserNameText();
		System.out.println("Last Visited Time:"+actualUserNametext);
	//	Assert.assertEquals(actualUserNametext, "Welcome, Mohammed Salahuddin Abu Helaluddin");
		
	
	}
	
	
	
	
//	@AfterMethod
//	public void tearDown(){
//		driver.quit();
//	}
	
	

}
