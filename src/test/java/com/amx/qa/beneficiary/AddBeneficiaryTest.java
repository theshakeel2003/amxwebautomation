package com.amx.qa.beneficiary;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.amx.qa.base.TestBase;
import com.amx.qa.pages.BankDetailsPage;
import com.amx.qa.pages.BeneficiariesPage;
import com.amx.qa.pages.BeneficiaryDetailsPage;
import com.amx.qa.pages.CountryAndChannelPage;
import com.amx.qa.pages.DashBoardPage;
import com.amx.qa.pages.LoginPage;
import com.amx.qa.pages.SearchBranchPage;
import com.amx.qa.pages.SecurityIdentityPage;

public class AddBeneficiaryTest extends TestBase{
	LoginPage loginPage;
	SecurityIdentityPage securityIdentityPage;
	DashBoardPage dashBoardPage;
	BeneficiariesPage beneficiariesPage;
	BankDetailsPage bankDetailsPage;
	CountryAndChannelPage countryAndChannelPage;
	SearchBranchPage searchBranchPage;
	BeneficiaryDetailsPage bneficiariesDetailsPage;


	//test cases should be separated -- independent with each other
	//before each test case -- launch the browser and login
	//@test -- execute test case
	//after each test case -- close the browser
	
	
	public AddBeneficiaryTest(){
			super();
			
	}
	
	
	@BeforeMethod
	public void setUp() {
		
		initialization();
		loginPage = new LoginPage();
	//	securityIdentityPage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		securityIdentityPage.clickOnVerifyCaption();
	//	dashBoardPage=securityIdentityPage.clickOnNextButton("test");
		beneficiariesPage=dashBoardPage.clickOnBeneficiaryLink();
		beneficiariesPage.waitForloader(driver, 30);
		countryAndChannelPage=beneficiariesPage.clickOnAddBeneficiary();
		
		
	}
	
	
	@Test(priority=1)
	public void verifyAddBeneficiaryTest() throws InterruptedException{
		
		
	
		countryAndChannelPage.selectChannelCountry("INDIA");
		bankDetailsPage=countryAndChannelPage.clickOnNextButton();
		
		bankDetailsPage.createBanDetails("AMX CASH INDIA", "EURO -Euro", "12345678901", "12345678901", "NRE ACCOUNT");
		
		searchBranchPage=bankDetailsPage.clickOnSearchBranch();
		
		searchBranchPage.searchBranch("AASIF COMMUNICATION");
		searchBranchPage.selectBranch();
		
		bneficiariesDetailsPage=bankDetailsPage.clickOnNextButton();
		bneficiariesDetailsPage.selectBeneficiaryType("Self");
		bneficiariesDetailsPage.createBeneficiaryDetails("SHAKEEL", "SHAIKH", "Wife", "Iraqi", "IRAQ", "BAGHDAD", "OTHERS", "+964","9819792");
		bneficiariesDetailsPage.clickOnNextButton();
		
	}	
	

	//@AfterMethod
//	public void tearDown(){
	//	driver.quit();
//	}
	
	
	
	
}
